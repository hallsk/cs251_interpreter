/*
parseFunctions.h
All functions needed for parser to run
Karen Halls and Sophia Davis
*/

#include "../tokenizer/tokenizeFunctions.h"

Value *parse(Value *tokens, Value *parsed, int* depth, int* completed);

Value *pushToStack(Value *tree, Value *token, int* depth);

Value *popFromStack(Value *tree, int* depth);

void freeTree(Value *tree);

Value *memCopyTree(Value *tree);

Value *memCopyValue(Value *car);