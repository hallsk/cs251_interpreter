/*
parser.c
A program that takes a string of tokens representing Scheme S-expressions 
(from our tokenize function) and produces a parse tree, if possible.
Program exits if there is a syntax error.
Sophia Davis and Karen Halls
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "parseFunctions.h"

// Parses tokenized input
Value *parse(Value *tokens, Value *parsed, int* depth, int* completed) {

	Value *topToken = tokens;
	
	Value *tree = parsed;
        
	while(topToken->type != endType) {

		Value *toGoToken = topToken;
		ConsCell *cell = topToken->cons;
		
		// if a token is not ')', add to tree
		if (cell->car->type != closeType) {
			if (cell->car->type == openType) { 
				(*depth)++;
			}
			tree = pushToStack(tree, cell->car, depth);
		}
		// if a token is ')'...
		else {
			// first check to make sure there aren't too many )'s
			(*depth)--;
			if (*depth < 0) {
				printf("syntax error -- depth\n");
				
				tree = NULL;
				return tree;
			}
			
			// and if not, pop appropriate tokens from tree, then add them as a branch
			tree = popFromStack(tree, depth);
		}
		
		
		// If we ever reach the end of an S-exp, and there are more tokens -- trigger error.
		if (*depth == 0 && cell->cdr->type != endType) {
            printf("syntax error -- end of S-exp with more tokens\n");

            tree = NULL;
            return tree;
        }
        // When we reach the end of an S-exp, and all parens are balanced, print what we have.
        if (*depth == 0 && *completed == 0) {
            *completed = 1;
        }
		topToken = cell->cdr;

	}
	
	
	return tree;
}

// Adds individual tokens -- as ConsCells -- to a tree
Value *pushToStack(Value *tree, Value *val, int* depth) {
	tree = insertConsCell(tree, val);
	return tree;
}

// Removes contents of ( ) from tree, then adds them to the tree as a miniTree
Value *popFromStack(Value *tree, int* depth) {
	Value *miniTree = ourMalloc(sizeof(Value));
	miniTree->type = endType;
	ConsCell *backupCell = tree->cons;
	
	while(backupCell->car->type != openType) {
		ConsCell *toGoCell = backupCell;
		Value *toGoVal = tree;
				
		miniTree = insertConsCell(miniTree, backupCell->car);
		tree = backupCell->cdr;
		
		backupCell = tree->cons;
	}
	
	Value *toGoVal = tree;
	tree = backupCell->cdr;

	tree = pushToStack(tree, miniTree, depth);
	return tree;	
}


// Frees all memory allocated in tree structure
void freeTree(Value *tree) {
	if (tree == NULL) {
	    return;
	}
	else if (tree->type == endType) {
	    free(tree);
	    return;
	}
	
	int empty = 0;
    
	while(!empty) {
		ConsCell *cell = tree->cons;

		// val cons -- move right
		if(cell->car->type != consType && cell->cdr->type != endType) {
			Value *toGo = tree;
			tree = cell->cdr;
			
			// Free any arrays stored in ConsCell
			if (cell->car->type == stringType){
				free(cell->car->string);
			}
			else if (cell->car->type == symbolType){
				free(cell->car->symbol);
			}
            
			free(toGo);
			free(cell->car);
			free(cell);
		}
		// val end -- free cell
		else if (cell->car->type != consType && cell->cdr->type == endType) { 
		
			// Free any arrays stored in ConsCell
			if (cell->car->type == stringType){
				free(cell->car->string);
			}
			else if (cell->car->type == symbolType){
				free(cell->car->symbol);
			}
			
			free(cell->car);
			free(cell->cdr);
			free(tree);
			free(cell);
			
			empty = 1;
		}
		// cons end -- free and go down
		else if (cell->car->type == consType && cell->cdr->type == endType) {
			freeTree(cell->car);
            
			free(cell->cdr);
			free(tree);
			free(cell);
			
			empty = 1;
		}
		// cons cons -- free below and go right later
		else {
			freeTree(cell->car);
			
			Value *toGo = tree;
			tree = cell->cdr;
            
			free(toGo);
			free(cell);
		}
	}
}

Value *memCopyTree(Value *tree) {
	if (tree->type == endType) {
        return tree;
    }
	ConsCell *cell;

	cell = tree->cons;
	if (cell->car->type == consType && cell->cdr->type == endType) {
		tree = memCopyTree(cell->car);
	}
	else if (cell->car->type == consType && cell->cdr->type == consType) {
		tree = memCopyTree(cell->car);
		tree = memCopyTree(cell->cdr);
	}
	else if (cell->car->type != consType && cell->cdr->type == consType) {
		
		tree = memCopyTree(cell->cdr);
		Value *newVal = memCopyValue(cell->car);
		Value *newTree = insertConsCell(tree, newVal);
		return newTree;
	}
	else if (cell->car->type != consType && cell->cdr->type == endType) {
		Value *newTree = ourMalloc(sizeof(Value));
		newTree->type = endType;
		Value *newVal = memCopyValue(cell->car);
		newTree = insertConsCell(newTree, newVal);
		return newTree;
	}
	
	return tree;
}

Value *memCopyValue(Value *car) {

	Value *newVal = ourMalloc(sizeof(Value));
	
	if (car->type != symbolType && car->type != stringType) {
		memcpy(newVal, car, sizeof(Value));
	}
	else {
		newVal->type = car->type;
		int i = 0;
		if (newVal->type == symbolType) {
			newVal->symbol = ourMalloc(sizeof(car->symbol));
			while(car->symbol[i] != '\0') {
				newVal->symbol[i] = car->symbol[i];
				i++;
			}
			newVal->symbol[i] = car->symbol[i];
		}
		else {

			newVal->string = ourMalloc(sizeof(car->string)*2);
			while(car->string[i] != '\0') {
				newVal->string[i] = car->string[i];
				i++;
			}
			newVal->string[i] = car->string[i];
		}
	}
	return newVal;
}