/*
parser_main.c
Reads input from a file, tokenizes the input, and tests our parse function.
Sophia Davis and Karen Halls
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

#include "parseFunctions.h"

int main(int argc, char *argv[]) {
    Value *tokens;
    
    int *completed = ourMalloc(sizeof(int));
    *completed = 0;
    
    int *depth = ourMalloc(sizeof(int));
    *depth = 0;
    
    int *tokenizeError = ourMalloc(sizeof(int));
    *tokenizeError = 0; 
    
    Value *reversed;
 
    Value *parsed = ourMalloc(sizeof(Value));
    parsed->type = endType;
    
    char *expression = ourMalloc(256 * sizeof(char));
    while (fgets(expression, 256, stdin)) {
    	
        tokens = tokenize(expression, tokenizeError);
        
        if (*tokenizeError == 1) {
            freeTokens(tokens);
            break;
        }



        reversed = reverseTokens(tokens);
        printTokens(reversed);
        parsed->type = endType;
        parsed = parse(reversed, parsed, depth, completed);
        
        // If we've found too many close parens or there's more than one S-exp on a line
        //  then stop reading input
        if (parsed == NULL) {
        	freeTree(parsed);
            break;
        }
        if (parsed->type == endType) {
        	freeTree(parsed);
        }
        if (*completed == 1) {
        	freeTree(parsed);
            *completed = 0;
            *depth = 0;
        }
    }
    
    // If there are too many open parens after all input has been read, trigger error.
    if (*completed == 0 && *depth > 0) {
        printf("syntax error\n");
    }
    free(expression);
    free(completed);
    free(depth);
    free(tokenizeError);
    return 0;
}
