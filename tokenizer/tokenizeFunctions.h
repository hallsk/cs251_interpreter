/*
tokenizeFunctions.h
All functions needed for tokenizer to run (includes ConsList functions)
Karen Halls and Sophia Davis
*/

#include "consList.h"

/*
Implemented in tokenizer.c
*/
Value *tokenize(char *expression, int* tokenizeError);

Value *insertConsCell(Value *list, Value *cellValue);

void printTokens(Value *tokens);

void freeTokens(Value *list);


/*
Implemented in consList.c
*/
void printCell(ConsCell *cell);

Value *reverseTokens(Value *cell);

void *ourMalloc(int size);

void printTree(Value *tree);

/*
Implemented in parseNumbers.c
*/
int isSign(char c);

int isNumber(char *possibleNumber);

int isUreal(char *possibleUreal);

int isUinteger(char *possibleUinteger);

int isUdecimal(char *possibleUdecimal);

/*
Implemented in parseText.c
*/
char *escape(char *unescaped);

char *unescape(char *escaped);

int isValidSymbolChar(char c);
