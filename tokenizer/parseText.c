/*
parseText.c
All functions that have to do with parsing text
Karen Halls and Sophia Davis
*/

#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "tokenizeFunctions.h"

/*
Checks whether a character is a valid symbol character in Scheme
*/
int isValidSymbolChar(char c) {
    if (isalpha(c)) {
        return 1;
    }
    else if (isdigit(c)) {
        return 1;
    }
    else if (c == '!' || c == '$' || c == '%' || c == '&' || c ==  '*' || c == '/' || c == ':' ||
                c == '<' || c == '=' || c == '>' || c == '?' || c == '~' || c == '_' || c == '^' ||
                c == '.' || c == '+' || c == '-') {
                return 1;
    }
    else {
        return 0;
    }  
}

/* This method replaces special characters in the string with their
   escaped two-character versions. Written by Karen Halls.  */
char *escape(char *unescaped) {

    int escapedSize = 0;
    
    // Counts how much of an increase there needs to be in array size, based on
    // special characters.  For each special character, the array needs to
    // increase 1.
    for (int i=0; i<strlen(unescaped); i++) {
        if (unescaped[i] == '\n' || unescaped[i] == '\\' || 
            unescaped[i] == '\'' || unescaped[i] == '\t' || unescaped[i] == '\"') {
            escapedSize++;
        }
    }
    
    escapedSize = escapedSize + strlen(unescaped) + 1;
    char *escaped = ourMalloc(escapedSize * sizeof(char));
    
    // Filling in escaped
    // If there is a special character in unescaped,
    // put '\\' in a spot, and then the spot afterwards, but the appropriate
    // character
    int j=0;
    for (int i=0; i<strlen(unescaped); i++) {
        switch (unescaped[i]) {
            case '\n':
                escaped[j] = '\\';
                j++;
                escaped[j] = 'n';
                j++;
                break;
            case '\\':
                escaped[j] = '\\';
                j++;
                escaped[j] = '\\';
                j++;
                break;
            case '\'':
                escaped[j] = '\\';
                j++;
                escaped[j] = '\'';
                j++;
                break;
            case '\t':
                escaped[j] = '\\';
                j++;
                escaped[j] = 't';
                j++;
                break;
            case '\"':
                escaped[j] = '\\';
                j++;
                escaped[j] = '\"';
                j++;
                break;
            default:
                escaped[j] = unescaped[i];
                j++;
                break;
        }
    }
    escaped[j] = '\0';
    
    return escaped;
}


/* This method replaces escaped characters with their one-character
   equivalents. Written by Karen Halls. */
char *unescape(char *escaped) {

    int unescapedSize = strlen(escaped) + 1;
    
    // Counts how much of a decrease there needs to be in array size, based on
    // special characters.  Each time '\\' shows up, means that a special
    // character will go there, and it needs to be decreased by 1.
    for (int i=0; i<strlen(escaped); i++) {
        if (escaped[i] == '\\' && escaped[i+1] != '\\') {
            unescapedSize--;
        }
    }
        
    char *unescaped = ourMalloc(unescapedSize * sizeof(char));

    // Filling in unescaped.
    int j=0;
    for (int i=0; i<strlen(escaped); i++) {
        if (escaped[i] == '\\') {
            i++;
            switch (escaped[i]) {
                case 'n':
                    unescaped[j] = '\n';
                    j++;
                    break;
                case 't':
                    unescaped[j] = '\t';
                    j++;
                    break;
                default:
                    unescaped[j] = escaped[i];
                    j++;
                    break;
            }
        }
        else {
            unescaped[j] = escaped[i];
            j++;
        }
    }
    unescaped[j] = '\0';

    return unescaped;
}
