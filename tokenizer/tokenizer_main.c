/* tokenizer_main.c
*  By Sophia Davis and Karen Halls
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

#include "tokenizeFunctions.h"

int main(int argc, char *argv[]) {
    Value *tokens;
    Value *reversed;
    int *tokenizeError = ourMalloc(sizeof(int));
    *tokenizeError = 0;
    
	char *expression = ourMalloc(256 * sizeof(char));
    while (fgets(expression, 256, stdin)) {
        tokens = tokenize(expression, tokenizeError);
        reversed = reverseTokens(tokens);
        printTokens(reversed);
        
        // we don't need to free tokens if the list has been reversed
        freeTokens(reversed);
    }
    free(expression);
    free(tokenizeError);
    return 0;
}
