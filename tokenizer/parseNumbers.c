/*
parseNumbers.c
All functions that have to do with parsing numbers
This is an implementation of the BNF given on the assignment page.
Karen Halls and Sophia Davis
*/

#include <ctype.h>
#include <string.h>
#include <stdio.h>

#include "tokenizeFunctions.h"

int isNumber(char *possibleNumber) {
    if (isSign(possibleNumber[0]) && isUreal(&possibleNumber[1])) {
    	return 1;
    }
    else if (isUreal(&possibleNumber[0])) {
    	return 1;
    }
    else {
    	return 0;
    }
}

int isSign(char c) {
	if (c == '+' || c == '-') {
		return 1;
	}
	else {
		return 0;
	}
}

int isUreal(char *possibleUreal) {
	if (isUinteger(possibleUreal)) {
		return 1;
	}
	else if (isUdecimal(possibleUreal)) {
		return 1;
	}
	else {
		return 0;
	}
}

int isUinteger(char *possibleUinteger){
	if (strlen(possibleUinteger) >= 1) {
		for (int i = 0; i < strlen(possibleUinteger); i++) {
			if (!(isdigit(possibleUinteger[i]))) {
				return 0;
			}
		}
		return 1;
	}
	return 0;
}

int isUdecimal(char *possibleUdecimal){
	if (possibleUdecimal[0] == '.' && isUinteger(&possibleUdecimal[1])) {
		return 1;
	}
	else {
		int i = 0;
		
		// check up to first period
		while (i < (strlen(possibleUdecimal) - 1) && possibleUdecimal[i] != '.') {
			if (!(isdigit(possibleUdecimal[i]))) {
				return 0;
			}
			i++;
		}
		if (possibleUdecimal[i] == '.') {
			if (i == (strlen(possibleUdecimal) - 1)) { 
				return 1;
			}
			else if (isUinteger(&possibleUdecimal[i + 1])) {
				return 1;
			}
		}
	}
	return 0;
}