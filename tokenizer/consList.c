/*
consList.c
All functions for forming and printing ConsCells in ConsCell-Value units (used by tokenizer.c)
Karen Halls and Sophia Davis
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tokenizeFunctions.h"

/*
Prints a single ConsCell and its value.
Mainly for debugging.
*/
void printCell(ConsCell *cell){

	char *escaped;

    switch(cell->car->type) {
        case openType:
            printf("%c ", cell->car->openParen);
            break;
        case closeType:
            printf("%c ", cell->car->closeParen);
            break;
        case stringType:
        	escaped = escape(cell->car->string);
            printf("\"%s\" ", escaped);
            break;
        case integerType:
            printf("%i ", cell->car->integerValue);
            break;
        case floatType:
            printf("%f ", cell->car->floatValue);
            break;
        case symbolType:
            printf("%s ", cell->car->symbol);
            break;
        case quoteType:
			printTree(cell->car);
			break;
        case booleanType:
            if (cell->car->booleanValue == 1) {
            	printf("#t ");
            }
            else {
            	printf("#f ");
            }
            break;
        case pointerType:
        	printf("%p ", cell->car->pointer);
            break;
        case consType:
        	printf("( ");
            break;
        case endType:
        	printf(") ");
            break;
        default:
            break;
    }
}

/*
Reverses ConsCell list so that it is in the same order as the original input string
*/
Value *reverseTokens(Value *oldListPtr){
	Value *previous = ourMalloc(sizeof(Value));
	previous->type = endType;
	
	while(oldListPtr->type != endType){
		ConsCell *toGoCons = oldListPtr->cons;
		Value *toGoVal = oldListPtr;
		Value *oldVal = oldListPtr->cons->car;

		if (oldVal->type == stringType){
			Value *newVal = ourMalloc(sizeof(Value));
        	newVal->type = stringType;
			char *copiedString = ourMalloc((strlen(oldVal->string) + 1)*sizeof(char));
			strcpy(copiedString, oldVal->string);
			newVal->string = copiedString;
			previous = insertConsCell(previous, newVal);
        }
        else if (oldVal->type == symbolType){
        	Value *newVal = ourMalloc(sizeof(Value));
        	newVal->type = symbolType;
            char *copiedSymbol = ourMalloc((strlen(oldVal->symbol) + 1)*sizeof(char));
			strcpy(copiedSymbol, oldVal->symbol);
			newVal->symbol = copiedSymbol;
			previous = insertConsCell(previous, newVal);
        }
		else {
			previous = insertConsCell(previous, oldVal);
		}
		
		oldListPtr = oldListPtr->cons->cdr;
	}
	
	return(previous);
}

// Prints every ConsCell in a tree structure, in correct order
void printTree(Value *tree) {

    if (tree->type == endType) {
        return;
    }
	
	ConsCell *cell;

	cell = tree->cons;
	if (cell->car->type == consType && cell->cdr->type == endType) {
		printf(" (");
		printTree(cell->car);
		printf(") ");
	}
	else if (cell->car->type == consType && cell->cdr->type == consType) {
		printf(" (");
		printTree(cell->car);
		printf(") ");
		printTree(cell->cdr);
	}
	else if (cell->car->type != consType && cell->cdr->type == consType) {
		printCell(cell);
		printTree(cell->cdr);
	}
	else if (cell->car->type != consType && cell->cdr->type == endType) {
		printCell(cell);
	}
}

/*
Keeps track of all memory malloc'd in global list.
All memory is freed before the program exits.
Called just like malloc: *pointer = ourMalloc(sizeof(Value));
*/
void *ourMalloc(int size) {
	void *address = malloc(size);
	
	Value *car = malloc(sizeof(Value));
	car->type = pointerType;
	car->pointer = address;	
	
	ConsCell *newCons = malloc(sizeof(ConsCell));
    newCons->car = car;
    newCons->cdr = garbageList;
    
    Value *previous = malloc(sizeof(Value));
    previous->type = consType;
    previous->cons = newCons;	
    
    garbageList = previous;
	return address;
}