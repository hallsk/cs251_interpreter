#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "tokenizeFunctions.h"

/*
Separates tokens in string and stores them in a list of ConsCell/Value units (in reverse order
from original input string)
*/
Value *tokenize(char *expression, int* tokenizeError) {

    int length = strlen(expression);

    Value *previous = ourMalloc(sizeof(Value));
    previous->type = endType;
    
    Value *val;
        
    int i = 0;
    while(i < length) {
        val = ourMalloc(sizeof(Value));
        switch(expression[i]) {
        	case '\n':
        		i++;
        		//free(val);
        		continue;
        	case '\t':
        		i++;
        		//free(val);
        		continue;
            case ' ':
                i++;
                //free(val);
                continue;
            case '(':
                val->type = openType;
                val->openParen = '(';
                i++;
                break;
            case '[':
                val->type = openType;
                val->openParen = '[';
                i++;
                break;
            case ')':
                val->type = closeType;
                val->closeParen = ')';
                i++;
                break;
            case ']':
                val->type = closeType;
                val->closeParen = ']';
                i++;
                break;
            case ';':
                //free(val);
                i = length;
                continue;
            case '\'':
            	val->type = quoteType;
            	val->quoteValue = '\'';
            	i++;
            	break;
            case '#':
                val->type = booleanType;
                i++;
                if (expression[i] == 't') {
                    if (i == (length - 1)) {
                        val->booleanValue = 1;
                    }
                	else if (i < (length - 1) && (expression[i+1] == ' ' || 
						expression[i+1] == ')' || expression[i+1] == '(' || 
						expression[i+1] == '[' || expression[i+1] == ']' || 
						expression[i+1] == ';' || expression[i+1] == '"' || 
						expression[i+1] == '\'' || expression[i+1] == '\n')) {

						val->booleanValue = 1;
                	}
                	else {
                		printf("syntax error\n");
                		*tokenizeError = 1;
                		//freeTokens(previous);
						val->type = endType;
						return val;
                	}
                }
                else if (expression[i] == 'f') {
                    if (i < (length - 1) && (expression[i+1] == ' ' || 
						expression[i+1] == ')' || expression[i+1] == '(' || 
						expression[i+1] == '[' || expression[i+1] == ']' || 
						expression[i+1] == ';' || expression[i+1] == '"' || 
						expression[i+1] == '\'' || expression[i+1] == '\n')) {

						val->booleanValue = 0;
                	}
                	else {
                		printf("syntax error\n");
                		*tokenizeError = 1;
                		//freeTokens(previous);
						val->type = endType;
						return val;
                	}
                }
                else {
                	printf("syntax error\n");
                	*tokenizeError = 1;
                	//freeTokens(previous);
         			val->type = endType;
         			return val;
                }
            
                i++;
                break;
            case '"':
                val->type = stringType;
                char *newString = ourMalloc((length - i)*sizeof(char));
                int strCounter = 0;
                i++;
                
                while(expression[i] != '"') {
                    newString[strCounter] = expression[i];
                    i++;
                    strCounter++;
                }
                newString[strCounter] = '\0';
                                              
                char *newStringUnescaped = unescape(newString);
                //free(newString);
                val->string = newStringUnescaped;
                
                i++;
                break;
            	
            default:
            	// default case deals with symbols and numbers -- which are parsed identically
            	// then categorized correctly (the set of characters which can make up numbers
            	// is a subset of characters which can make up symbols)
                if (isValidSymbolChar(expression[i])) {
                    char *newSymbol = ourMalloc((length - i)*sizeof(char) + 1);
                    int strCounter = 0;
                	
                	// space, parens, brackets, ';', and \n all mark end of symbol/number
                    while((i != length) && (expression[i] != ' ' && expression[i] != '(' && expression[i] != '['
                        && expression[i] != ')' && expression[i] != ']' && expression[i] != ';'
                        && expression[i] != '\'' && expression[i] != '\n')) {
                        
                        newSymbol[strCounter] = expression[i];
                        i++;
                        strCounter++;
                    }
                    newSymbol[strCounter] = '\0';
                                
                    if (isNumber(newSymbol)) {
                        int sign = 0;
                        if (newSymbol[0] == '-' || newSymbol[0] == '+') {
                            sign = 1;
                        }
                        
                        // If sign == 0, we can start the check from beginning of string
                        // If sign == 1, we must start the check starting after the sign,
                        // i.e. index 1
                        if (isUinteger(&newSymbol[sign])) {
                            val->type = integerType;
                            val->integerValue = atoi(newSymbol);
                            //free(newSymbol); // maybe remove?????
                        }
                        else if (isUdecimal(&newSymbol[sign])) {
                            val->type = floatType;
                            val->floatValue = atof(newSymbol);
                            //free(newSymbol);
                        }
                    }
                    else {
                        val->type = symbolType;
                        val->symbol = newSymbol;
                    }
                }
                else {
                	printf("syntax error\n");
                	*tokenizeError = 1;
                	//freeTokens(previous);
         			val->type = endType;
         			return val;
                }
                break;
        }
        previous = insertConsCell(previous, val);
        
    }
    return previous;
}

Value *insertConsCell(Value *list, Value *cellValue) {
    ConsCell *newCons = ourMalloc(sizeof(ConsCell));
    newCons->car = cellValue;
    newCons->cdr = list;
    
    Value *previous = ourMalloc(sizeof(Value));
    previous->type = consType;
    previous->cons = newCons;
    
    return previous;
}

/*
Prints each token, along with its type
*/
void printTokens(Value *list){

    ConsCell *cell;
    char *escaped;
    

    while(list->type != endType){
    
        cell = list->cons;
        switch(cell->car->type) {
            case openType:
                printf("%c:open\n", cell->car->openParen);
                break;
            case closeType:
                printf("%c:close\n", cell->car->closeParen);
                break;
            case stringType:
            	escaped = escape(cell->car->string);
                printf("\"%s\":string\n", escaped);
                //free(escaped);
                break;
            case integerType:
                printf("%i:integer\n", cell->car->integerValue);
                break;
            case floatType:
                printf("%f:float\n", cell->car->floatValue);
                break;
            case symbolType:
                printf("%s:symbol\n", cell->car->symbol);
                break;
            case quoteType:
            	printf("%c:symbol\n", cell->car->quoteValue);
                break;
            case booleanType:
            	if (cell->car->booleanValue == 0){
            		printf("#f:bool\n");
            	}
            	else if (cell->car->booleanValue == 1) {
            		printf("#t:bool\n");
            	}
                break;
            case pointerType:
            	printf("%p\n", cell->car->pointer);
                break;
            default:
                break;
        }
        list = list->cons->cdr;
    }
}

 
/*
Frees all space ourMalloc'd in creation of ConsCell list
*/
void freeTokens(Value *list) {
    Value *cdr = list;
    
    while(cdr->type != endType) {
        Value *toGo = cdr;
        ConsCell *cell = cdr->cons;
                
	    // Free any arrays stored in ConsCell
        if (cell->car->type == stringType){
            free(cell->car->string);
        }
        else if (cell->car->type == symbolType){
            free(cell->car->symbol);
        }
        else if (cell->car->type == pointerType) {
        	free(cell->car->pointer);
        }

        cdr = cell->cdr;
                
        free(toGo);
        free(cell->car);
        free(cell);
    }
    // and the last Value
    free(cdr);
}
