/*
consList.h
Types needed to form ConsCells and Values for tokenizer
Sophia Davis and Karen Halls
*/

typedef enum {
  booleanType,
  openType,
  closeType,
  stringType,
  integerType,
  floatType,
  symbolType,
  quoteType,
  voidType,
  consType,
  pairType,
  listType,
  bindingType,
  closureType,
  endType,
  endFrame,
  pointerType
} VALUE_TYPE;

typedef struct __Value {
  VALUE_TYPE type;
  union {
    char openParen;
    char closeParen;
    char *string;
    char *symbol;
    char quoteValue;
    int integerValue;
    float floatValue;
    int booleanValue;
    void *pointer;
    struct __ConsCell *cons;
    struct __Binding *nextBinding;
    struct __Closure *closure;
  };
} Value;

typedef struct __ConsCell {
    struct __Value *car;
    struct __Value *cdr;
} ConsCell;

typedef struct __Binding {
    char *name;
    struct __Value *val;
    struct __Value *nextBinding;
} Binding;

typedef struct __Closure {
    struct __Value *params;
    struct __Value *bodyTree;
    struct __Value *refFrame;
} Closure;
	
struct __Value *garbageList;