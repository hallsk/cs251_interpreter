/* interpreter.c
* Our main evaluating function
*  By Sophia Davis and Karen Halls
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "interpretFunctions.h"

/*
Evaluates an S-expression starting with the first non-linking ConsCell
*/
Value *eval(Value *expr, Value **envPointer, int checker) {

	ConsCell *cell;
	Value *env = *envPointer;
	
	if (expr->type != consType) {
		Value *evaluated = evalNaked(expr, env);
		return evaluated;
	}

	// cons end
	else if (expr->cons->car->type == consType && expr->cons->cdr->type == endType) {
		return eval(expr->cons->car, envPointer, checker);
	}
	// cons cons
	else if (expr->cons->car->type == consType && expr->cons->cdr->type == consType) {
		return eval(expr->cons->car, envPointer, checker);
	}
	else {
	
		ConsCell *cell = expr->cons;

		// For storing the values of resolved variables
		Value *resolved;
		
		// The evaluated ConsCell or S-expression (return value)
		Value *evaluated;
		
		switch (cell->car->type) {
			case endType:
				return NULL;
			case symbolType:
				if (!strcmp(cell->car->symbol, "if")) {
					if (cell->cdr->type != consType) {
						printf("syntax error -- no args in if exp\n");
						return NULL;
					}
					evaluated = evalIf(cell->cdr, env);
				}				
				else if (!strcmp(cell->car->symbol, "quote")) {
					evaluated = evalQuote(cell->cdr);
				}
				else if (!strcmp(cell->car->symbol, "null?")) {
					evaluated = eval(cell->cdr, envPointer, checker);
					evaluated = null(evaluated);
				}
				else if (!strcmp(cell->car->symbol, "car")) {
					evaluated = eval(cell->cdr, envPointer, checker);
					evaluated = car(evaluated);
				}
				else if (!strcmp(cell->car->symbol, "cdr")) {
					evaluated = eval(cell->cdr, envPointer, checker);
					evaluated = cdr(evaluated);
				}
				else if (!strcmp(cell->car->symbol, "+")) {
					evaluated = evalPlus(cell->cdr, env, checker);
				}
				else if (!strcmp(cell->car->symbol, "*")) {
					evaluated = mult(cell->cdr, env, checker);
				}
				else if (!strcmp(cell->car->symbol, "-")) {
					evaluated = subtraction(cell->cdr, env, checker);
				}
				else if (!strcmp(cell->car->symbol, "/")) {
					evaluated = division(cell->cdr, env, checker);
				}
				else if (!strcmp(cell->car->symbol, "modulo")) {
					evaluated = mod(cell->cdr, env, checker);
				}
				else if (!strcmp(cell->car->symbol, "<=")) {
					evaluated = leq(cell->cdr, env, checker);
				}
				else if (!strcmp(cell->car->symbol, ">=")) {
					evaluated = geq(cell->cdr, env, checker);
				}
				else if (!strcmp(cell->car->symbol, "=")) {
					evaluated = eq(cell->cdr, env, checker);
				}
				else if (!strcmp(cell->car->symbol, ">")) {
					evaluated = gt(cell->cdr, env, checker);
				}
				else if (!strcmp(cell->car->symbol, "<")) {
					evaluated = lt(cell->cdr, env, checker);
				}
				else if (!strcmp(cell->car->symbol, "and")) {
					evaluated = evalAnd(cell->cdr, env, checker);
				}
				else if (!strcmp(cell->car->symbol, "or")) {
					evaluated = evalOr(cell->cdr, env, checker);
				}
				else if (!strcmp(cell->car->symbol, "begin")) {
					evaluated = evalBegin(cell->cdr, env, checker);
				}
				else if (!strcmp(cell->car->symbol, "cond")) {
					evaluated = evalCond(cell->cdr, env, checker);
				}
				else if (!strcmp(cell->car->symbol, "cons")) {
					evaluated = evalCons(cell->cdr, env, checker);
				}
				else if (!strcmp(cell->car->symbol, "lambda")) {
					evaluated = addClosure(cell->cdr->cons->car, cell->cdr->cons->cdr, env);
				}
				else if (!strcmp(cell->car->symbol, "let")) {
					evaluated = addLet(cell->cdr, envPointer, checker, "let");
				}
				else if (!strcmp(cell->car->symbol, "let*")) {
					evaluated = addLet(cell->cdr, envPointer, checker, "letStar");
				}
				else if (!strcmp(cell->car->symbol, "letrec")) {
					evaluated = addLetRec(cell->cdr, envPointer, checker);
				}
				else if (!strcmp(cell->car->symbol, "set!")) {
					if (cell->cdr->type != consType) {
						printf("syntax error -- no args in set!\n");
						return NULL;
					}
					if (cell->cdr->cons->car->type != symbolType) {
						printf("syntax error -- no symbol to set!\n");
						return NULL;
					}
					if (cell->cdr->cons->cdr->type != consType) {
						printf("syntax error -- not enough args in set!\n");
						return NULL;
					}
					evaluated = setBang(cell->cdr->cons->car, cell->cdr->cons->cdr->cons->car, env);
				}
				// START DEFINE ~~~~~~~~~~~~
				else if (!strcmp(cell->car->symbol, "define")) {
					// define not followed by anything
					if (cell->cdr->type != consType) {
						printf("syntax error -- no args in define\n");
						return NULL;
					}
					if (cell->cdr->cons->car->type != symbolType) {
						printf("syntax error -- no symbol to define\n");
						return NULL;
					}
					if (cell->cdr->cons->cdr->type != consType) {
						printf("syntax error -- not enough args in define\n");
						return NULL;
					}
					
					env = *envPointer;
					env = addFrame(cell->cdr, env, env, "define");
					*envPointer = env;

					if (env == NULL) {
						evaluated = NULL;
						return evaluated;
					}
					evaluated = ourMalloc(sizeof(Value));
					evaluated->type = voidType;
				}// END DEFINE ~~~~~~~~~~~~~~
				
				else {
					resolved = resolveVariable(cell, env);
					if (resolved == NULL) {
						return resolved;
					}
					else if (resolved->type == closureType && !checker) {
						Value *params = resolved->closure->params;

						env = *envPointer;
						Value *currentEnv = mergeInRefFrame(resolved->closure->refFrame, env);
						if (params != NULL) {							
							Value *currParam;
							Value *toMatch = cell->cdr;

							Value *toMatchEvaled = eval(toMatch, &currentEnv, checker);
							
							while (params->cons->cdr->type != endType) {
								currParam = params->cons->car;
								currentEnv = addBinding(toMatchEvaled, currParam, currentEnv);
								params = params->cons->cdr;
								toMatch = toMatch->cons->cdr;
								toMatchEvaled = eval(toMatch, &currentEnv, checker);
							}
							currParam = params->cons->car;

							currentEnv = addBinding(toMatchEvaled, currParam, currentEnv);
						}
						resolved = eval(resolved->closure->bodyTree, &currentEnv, checker);

					}
					return resolved;
				}
				return evaluated;
			default:
				evaluated = evalNaked(cell->car, env);
				return evaluated;
		}
	}
}

/*
Evaluates values that point to bools, strings, ints, or floats
*/
Value *evalNaked(Value *nakedExpr, Value *env) {

	ConsCell *tempCell;
	Value *evaluated = ourMalloc(sizeof(Value));
	evaluated->type = nakedExpr->type;

	switch(nakedExpr->type) {
		
		case booleanType:
			evaluated->booleanValue = nakedExpr->booleanValue;
			break;
		case stringType:
			evaluated->string = nakedExpr->string;
			break;
		case integerType:
			evaluated->integerValue = nakedExpr->integerValue;
			break;
		case floatType:
			evaluated->floatValue = nakedExpr->floatValue;
			break;
		case symbolType:
			tempCell = ourMalloc(sizeof(ConsCell));
			tempCell->car = nakedExpr;
			evaluated = resolveVariable(tempCell, env);
			break;
		case closureType:
			evaluated = nakedExpr;
			break;
		case consType:
			evaluated = nakedExpr;
			break;
		case quoteType:
			evaluated = nakedExpr;
			break;
		case listType:
			evaluated = nakedExpr;
			break;
		case pairType:
			evaluated = nakedExpr;
			break;
		default:
			return NULL;
	}
	return evaluated;
}
