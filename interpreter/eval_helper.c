/* eval_helper.c
* Helper functions for evaluations.
*  By Sophia Davis and Karen Halls
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "interpretFunctions.h"



/*
Evaluates an if S-expression
*/
Value *evalIf(Value *expr, Value *env) {
	
	Value *response;
	ConsCell *nextCell = expr->cons;

	// if the if S-expression has 1 or 2 arguments
	if (nextCell->cdr->type == endType) {
		printf("syntax error-- not enough args in if\n");
		return NULL;
	}
	else if (nextCell->cdr->cons->cdr->type == endType) {
		printf("syntax error-- not enough args in if\n");
		return NULL;
	}
	
	// if the if S-expression has 3 arguments
	else if (nextCell->cdr->cons->cdr->cons->cdr->type == endType) {
		Value *evaled;
		if (nextCell->car->type == symbolType) {
			evaled = resolveVariable(nextCell, env);
		}
		else {
			evaled = eval(nextCell->car, &env, 0);
		}
		if (evaled == NULL) {
			return evaled;
		}
		int bool = truthy(evaled)->booleanValue;
		
		if (bool == 1) {
			if (nextCell->cdr->cons->car->type == consType) {
				response = eval(nextCell->cdr->cons->car, &env, 0);
			}
			else {
				response = eval(nextCell->cdr, &env, 0);
			}
		}
		else {
			response = eval(nextCell->cdr->cons->cdr, &env, 0);
		}
		return response;
	}
	// if the if S-expression has more than 3 arguments
	else {
		printf("syntax error-- too many args in if\n");
		return NULL;
	}
}

/*
Evaluates a quoted S-expression
*/
Value *evalQuote(Value *expr) {
	Value *response;
	response = expr;
	response->type = quoteType;
	
	return response;
}

/*
Adds a closure, containing the body of a function, its referencing environment, and its formal parameters
*/
Value *addClosure(Value *params, Value *body, Value *currentEnv) {
	
	Value *evaluated = ourMalloc(sizeof(Value));
	Closure *newClosure = ourMalloc(sizeof(Closure));
					
	newClosure->refFrame = ourMalloc(sizeof(Value));
	newClosure->params = ourMalloc(sizeof(Value));
	newClosure->bodyTree = ourMalloc(sizeof(Value));
	
	if (params->type == endType) {
		newClosure->params = NULL;
	}
	else {
		newClosure->params = evalQuote(params);
	}
	newClosure->refFrame = currentEnv;
	newClosure->bodyTree = body;
	
	evaluated->type = closureType;
	evaluated->closure = newClosure;
	
	return evaluated;
}

/*
Binds variables in let, let*, or define expressions
*/
Value *addLet(Value *currentParam, Value **envPointer, int checker, char *type) {

	Value *evaluated;
	Value *env = *envPointer;
	
	// let is not followed by anything
	if (currentParam->type == endType) {
		printf("syntax error -- no args in let\n");
		evaluated = NULL;
		envPointer = freeFrame(envPointer, checker);
		env = *envPointer;
		return evaluated;
	}
	//the first of the 2 parens is not there
	else if (currentParam->cons->car->type != consType) {
		printf("syntax error -- missing parentheses in let\n");
		evaluated = NULL;
		envPointer = freeFrame(envPointer, checker);
		env = *envPointer;
		return evaluated;
	}

	if (env->type != endType) {
		env->type = endFrame;
	}
/*
	Value *fake = ourMalloc(sizeof(Value));
	fake->type = voidType;
	env->type = endType;
	env = addBinding(fake, fake, env);*/
	env = addFrame(currentParam->cons->car, env, env, type);
	
	*envPointer = env;
	
	if (env == NULL) {
		evaluated = NULL;
		return evaluated;
	}
	
	evaluated = eval(currentParam->cons->cdr, envPointer, 0);
	
	// free previous frame after evaluation
	envPointer = freeFrame(envPointer, checker);
	env = *envPointer;
	
	// deal with freeing nested lets
	if (env->type == endFrame) {
		env->type = bindingType;
	}
	
	*envPointer = env;
	
	return evaluated;
}

/*
Binds variables in letrec expression
*/
Value *addLetRec(Value *currentParam, Value **envPointer, int checker) {

	Value *evaluated;
	Value *env = *envPointer;
	
	// letrec is not followed by anything
	if (currentParam->type == endType) {
		printf("syntax error -- no args in letrec\n");
		evaluated = NULL;
		envPointer = freeFrame(envPointer, checker);
		return evaluated;
	}
	//the first of the 2 parens is not there
	else if (currentParam->cons->car->type != consType) {
		printf("syntax error -- missing parentheses in letrec\n");
		evaluated = NULL;
		envPointer = freeFrame(envPointer, checker);
		return evaluated;
	}

	if (env->type != endType) {
		env->type = endFrame;
	}
	
	// set params to false
	env = addFrame(currentParam->cons->car, env, env, "letRecFalse");
	
	if (env == NULL) {
		evaluated = NULL;
		return evaluated;
	}
	
	// set temps to the actual values we want bound
	env = addFrame(currentParam->cons->car, env, env, "letRecTemp");
	
	if (env == NULL) {
		evaluated = NULL;
		return evaluated;
	}
	
	// change the params to be what we want bound
	env = addFrame(currentParam->cons->car, env, env, "set!");
	
	if (env == NULL) {
		evaluated = NULL;
		return evaluated;
	}
	
	*envPointer = env;
	
	evaluated = eval(currentParam->cons->cdr, envPointer, 0);
	
	// free previous frame
	envPointer = freeFrame(envPointer, checker);
	env = *envPointer;
	
	return evaluated;
}

/*
Evaluates 'and'
*/
Value *evalAnd(Value *currentParam, Value *env, int checker) {
	
	Value *toReturn = ourMalloc(sizeof(Value));
	toReturn->type = voidType; // keep track of if we never progress
	
	while(currentParam->type != endType){
		
		Value *resolved = eval(currentParam, &env, checker);
		
		if (resolved == NULL) {
			toReturn = NULL;
			return toReturn;
		}
		else if (truthy(resolved)->booleanValue) {
			toReturn = resolved;
			currentParam = currentParam->cons->cdr;
		}
		else {
			toReturn->type = booleanType;
			toReturn->booleanValue = 0;
			break;
		}
	}
	// if we hit an endType right away, then we have (and), which returns true
	if (toReturn->type == voidType) {
		toReturn->type = booleanType;
		toReturn->booleanValue = 1;
	}
	return toReturn;
}

/*
Evaluates 'or'
*/
Value *evalOr(Value *currentParam, Value *env, int checker) {
	
	Value *toReturn = ourMalloc(sizeof(Value));
	
	// evaluates to true only if an expression is truthy
	toReturn->type = booleanType;
	toReturn->booleanValue = 0;
	
	while(currentParam->type != endType){
		
		Value *resolved = eval(currentParam, &env, checker);
		
		if (resolved == NULL) {
			toReturn = NULL;
			return toReturn;
		}
		// if we find any truthy expression, the whole or-exp evals to true
		else if (truthy(resolved)->booleanValue) {
			toReturn = resolved;
			break;
		}
		else {
			currentParam = currentParam->cons->cdr;
		}
	}
	// if we hit an endType right away, then we have (or) or never found
	// anything truthy, which returns false
	return toReturn;
}

/*
Evaluates 'begin'
*/
Value *evalBegin(Value *currentParam, Value *env, int checker) {
	Value *resolved = ourMalloc(sizeof(Value));
	resolved->type = voidType; // if there's nothing to return
	
	while(currentParam->type != endType){
		resolved = eval(currentParam, &env, checker);
		currentParam = currentParam->cons->cdr;
	}
	return resolved;
}

/*
Evaluates 'cond'
*/
Value *evalCond(Value *currentParam, Value *env, int checker) {
	
	Value *toReturn = ourMalloc(sizeof(Value));
	toReturn->type = voidType; // keep track of if we never progress
	
	// (cond) -- returns nothing
	if (currentParam->type != consType) {
		return toReturn;
	}
	//the first of the 2 parens is not there
	else if (currentParam->cons->car->type != consType) {
		printf("syntax error -- not enough parens in cond\n");
		toReturn = NULL;
	}
	
	else {		
		while(currentParam->type != endType){
		
			Value *resolved = eval(currentParam, &env, checker);
		
			if (resolved == NULL) {
				toReturn = NULL;
				return toReturn;
			}
			// condition evaluates to true -- evaluate consequence
			else if (truthy(resolved)->booleanValue) {
				if (currentParam->cons->car->type == consType) {
					Value *next = currentParam->cons->car->cons->cdr;
					while (next->type != endType) {
						resolved = eval(next, &env, checker);
						next = next->cons->cdr;
					}
				}
				toReturn = resolved;
				break;
			}
			// condition evaluates to false -- skip to next condition
			else {
				// reached end of conditions -- they were all false
				if (currentParam->cons->cdr->type == endType) {
					break;
				}
				else {
					currentParam = currentParam->cons->cdr;
				}
			}
		}
	}
	return toReturn;
}