/* interpreter_main.c
*  By Sophia Davis and Karen Halls
*/

#include <stdio.h>
#include <stdlib.h>

#include "interpretFunctions.h"

int main(int argc, char *argv[]) {

	garbageList = malloc(sizeof(Value));
	garbageList->type = endType;
    
    //// for tokenizer
    Value *tokens;
    Value *reversed;
    
    //// for parser
    int *completed = ourMalloc(sizeof(int));
    *completed = 0;
    
    int *depth = ourMalloc(sizeof(int));
    *depth = 0;
    
    int *tokenizeError = ourMalloc(sizeof(int));
    *tokenizeError = 0; 
    
    Value *parsed = ourMalloc(sizeof(Value));
    parsed->type = endType;
    
    
    //// for interpreter
    Value **envPointer = ourMalloc(sizeof(Value));
	Value *env = ourMalloc(sizeof(Value));
	env->type = endType;
	
	*envPointer = env;
    
    Value *evaluated;
    
    char *expression = ourMalloc(256 * sizeof(char));
    while (fgets(expression, 256, stdin)) {
        tokens = tokenize(expression, tokenizeError);
        
        if (*tokenizeError == 1) {
            break;
        }
        reversed = reverseTokens(tokens);
        
        parsed = parse(reversed, parsed, depth, completed);
        
        // If we've found too many close parens or there's more than one S-exp on a line
        //  then stop reading input
        if (parsed == NULL) {
        	env = ourMalloc(sizeof(Value));
			env->type = endType;
        	parsed = ourMalloc(sizeof(Value));
    		parsed->type = endType;
            break;
        }
        
        // if line contains only a comment or is empty
        if (parsed->type == endType) {
        	if (env->type == endType) {
				env = ourMalloc(sizeof(Value));
				env->type = endType;
			}
        	parsed = ourMalloc(sizeof(Value));
    		parsed->type = endType;
        }
        
        // if we're still reading in input
        else if(*completed==0) {
        	if (env->type == endType) {
				env = ourMalloc(sizeof(Value));
				env->type = endType;
			}
        }
        
        else if (*completed == 1) {
        	evaluated = eval(parsed, envPointer, 0);
        	if (evaluated != NULL) {
				if (evaluated->type != voidType) {		
					if (evaluated->type == quoteType || evaluated->type == consType) {
						if (null(evaluated)->booleanValue) {
							printf("(");
						}
						printValue(evaluated);
					}
					else if (evaluated->type == listType) {
						printf("(");
						printValue(evaluated);
						printf(")");
					}
					else {
						printValue(evaluated);
					}
					printf("\n");
				}
			}
			else {
				break;
			}
            *completed = 0;
            parsed = ourMalloc(sizeof(Value));
    		parsed->type = endType;
        }
    }
    
    // If there are too many open parens after all input has been read, trigger error.
    if (*completed == 0 && *depth > 0) {
        printf("syntax error\n");
    }
    freeTokens(garbageList);    
    return 0;
}
    	

        
