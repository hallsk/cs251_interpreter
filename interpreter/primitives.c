/* primitives.c
*  By Sophia Davis and Karen Halls
* Implementations of primitive functions in Scheme
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "interpretFunctions.h"

/*
Determines whether a list is empty
*/
Value *null(Value *list) {
	
	Value *boolVal = ourMalloc(sizeof(Value));
	boolVal->type = booleanType;
	
	if (list == NULL) {
		return NULL;
	}

	// if its not a list, then some value is there -- return false
	if(list->type != quoteType && list->type != consType) {
		boolVal->booleanValue = 0;
	}
	// otherwise its a list -- check if it's empty
	else if (list->cons->car->type == endType && list->cons->cdr->type == endType) {
		boolVal->booleanValue = 1;
	}
	else {
		boolVal->booleanValue = null(list->cons->car)->booleanValue;
	}
	return boolVal;
}

/*
Gets the car of a list or pair, returns syntax error if not possible (aka not a list)
*/
Value *car(Value *list) {
	if (list->type == consType || list->type == listType || list->type == quoteType || list->type == pairType) {
		if (list->cons->car->type == consType || list->cons->car->type == listType || list->cons->car->type == quoteType) {
			return list->cons->car->cons->car;
		}
		else {
			return list->cons->car;
		}
	}
 	else {
 		return list;
 	}
}

/*
Gets the cdr of a list or pair, and returns a syntax error if not a list or pair, or otherwise can't
find the cdr
*/
Value *cdr(Value *list) {
	Value *newList = ourMalloc(sizeof(Value));
	ConsCell *newCons = ourMalloc(sizeof(ConsCell));
	
	newCons->cdr = ourMalloc(sizeof(Value));
	newCons->cdr->type = endType;
	
	if (list->type == pairType) {
		return list->cons->cdr;
	}
	
	// cdr of non-list --> syntax error
	else if (list->type != quoteType && list->type != consType && list->type != listType) {
		printf("syntax error -- parameter of cdr is not a list\n");
		return NULL;
	}
	else {
		while (list->cons->car->type == consType) {
			list = list->cons->car;
		}

		newCons->car = ourMalloc(sizeof(Value));
		newCons->car = list->cons->cdr;
		newList->type = consType;
		newList->cons = newCons;
		return newList;
	}
}


Value *evalCons(Value *currentTree, Value *env, int checker) {
	Value *newCar;
	
	//resolve vars if there are symbols or ints in car
	if (currentTree->cons->car->type != consType) {
		if (currentTree->cons->car->type == symbolType) {
			newCar = resolveVariable(currentTree->cons, env);
			
			if (newCar == NULL) {
				return NULL;
			}
		}
		else {
			newCar = eval(currentTree, &env, checker);
		}
	}
	
	// if cons type, eval car
	else {
		newCar = eval(currentTree->cons->car, &env, checker);
	}

	// need to double check that there are 2 params
	if (currentTree->cons->cdr->type != consType) {
		printf("syntax error -- cons, not enough params\n");
		return NULL;
	}
	
	Value *newCdr;
	// eval and resolve cdr
	if (currentTree->cons->cdr->cons->car->type != consType) {
		if (currentTree->cons->cdr->cons->car->type == symbolType) {
			newCdr = resolveVariable(currentTree->cons->cdr->cons, env);
			if (newCdr == NULL) {
				return NULL;
			}
		}
		else {
			newCdr = currentTree->cons->cdr;
		}
	}
	// if cons type, eval cdr
	else {
		newCdr = eval(currentTree->cons->cdr, &env, checker);
	}

	Value *temp;
	int tempInit = 0;
	
	if (!(newCar->type == consType || newCar->type == pairType 
				|| newCar->type == quoteType || newCar->type == listType)) {

		ConsCell *newCell = ourMalloc(sizeof(ConsCell));
		
		newCell->car = newCar;
		newCell->cdr = ourMalloc(sizeof(Value));
		newCell->cdr->type = endType;
		
		temp = ourMalloc(sizeof(Value));
		temp->type = consType;
		temp->cons = newCell;
		tempInit = 1;
	}
	
	Value *newVal;
	if (!tempInit) {
		temp = newCar;
	}
	
	// takes care of cons onto an empty list
	if (null(newCdr)->booleanValue) {
		newVal = temp;
		newVal->type = listType;
		return newVal;
	}
	
	int tempProgress = 0;
	
	if ((newCdr->type == consType || newCdr->type == quoteType || newCdr->type == listType) && newCdr->cons->car->type == consType) {		
		while (!(temp->cons->cdr->type == endType)) {
			tempProgress = 1;
			temp = temp->cons->cdr;
		}
		temp->cons->cdr->type = consType;
		
		temp->cons->cdr->cons = ourMalloc(sizeof(ConsCell));
		temp->cons->cdr->cons = newCdr->cons->car->cons;

		if (tempProgress) {
			newVal = newCar;
		}
		else {
			newVal = temp;
		}
		newVal->type = listType;
	}
	
	// takes care of pairs
	else {
		ConsCell *anotherNewCell = ourMalloc(sizeof(ConsCell));
		anotherNewCell->car = newCar;
		if (newCdr->type == listType || newCdr->type == pairType) {
			anotherNewCell->cdr = newCdr;
		}
		else {
			if (newCdr->type == consType) {
				anotherNewCell->cdr = newCdr->cons->car;
			}
			else {
				anotherNewCell->cdr = newCdr;
			}
		}
		newVal = ourMalloc(sizeof(Value));
		newVal->cons = anotherNewCell;
		if (newCdr->type == listType || newCdr->type == pairType) {
			newVal->type = listType;
		}
		else {
			newVal->type = pairType;
		}
	}

	return newVal;
}

/*
Addition
*/
Value *evalPlus(Value *currentParam, Value *env, int checker) {

	Value *evaluated = ourMalloc(sizeof(Value));
	
	double currentValue = 0;
	int floatCheck = 0;
	
	while(currentParam->type != endType){
		Value *resolved = eval(currentParam, &env, checker);
		
		if (resolved == NULL) {
			evaluated = NULL;
			return evaluated;
		}
		else if (resolved->type == integerType) {
			currentValue += resolved->integerValue;
		}
		else if (resolved->type == floatType) {
			currentValue += resolved->floatValue;
			floatCheck = 1;
		}
		else {
			printf("syntax error -- non-numeric parameter\n");
			evaluated = NULL;
			return evaluated;
		}
		currentParam = currentParam->cons->cdr;
	}
	if (floatCheck) {
		evaluated->type = floatType;
		evaluated->floatValue = currentValue;
	}
	else {
		evaluated->type = integerType;
		evaluated->integerValue = currentValue;
	}
	return evaluated;
}

/*
Multiplication
*/
Value *mult(Value *currentParam, Value *env, int checker) {

	Value *evaluated = ourMalloc(sizeof(Value));
	
	double currentValue = 1;
	int floatCheck = 0;
	
	// if just pass in * with no parameters, then it should return 1
	if (currentParam->type == endType) {
		evaluated->type = integerType;
		evaluated->integerValue = currentValue;
		return evaluated;
	}
	while(currentParam->type != endType){
		Value *resolved = eval(currentParam, &env, checker);
		
		if (resolved == NULL) {
			evaluated = NULL;
			return evaluated;
		}
		else if (resolved->type == integerType) {
			currentValue = resolved->integerValue * currentValue;
		}
		else if (resolved->type == floatType) {
			currentValue = resolved->floatValue * currentValue;
			floatCheck = 1;
		}
		else {
			printf("syntax error -- non-numeric parameter\n");
			evaluated = NULL;
			return evaluated;
		}
		currentParam = currentParam->cons->cdr;
	}
	if (floatCheck) {
		evaluated->type = floatType;
		evaluated->floatValue = currentValue;
	}
	else {
		evaluated->type = integerType;
		evaluated->integerValue = currentValue;
	}
	return evaluated;
}

/*
Subtraction
*/
Value *subtraction(Value *currentParam, Value *env, int checker) {

	Value *evaluated = ourMalloc(sizeof(Value));
	
	double currentValue = 0;
	int floatCheck = 0;
	
	// checks for too few and too many parameters
	if (currentParam->type == endType || currentParam->cons->cdr->type == endType ||
			currentParam->cons->cdr->cons->cdr->type != endType) {
		printf("syntax error -- wrong number of parameters\n");
		return NULL;
	}
	
	Value *resolved = eval(currentParam, &env, checker);
	Value *resolved2 = eval(currentParam->cons->cdr, &env, checker);
	
	// couldn't find the variable, error has already been printed
	if (resolved == NULL || resolved2 == NULL) {
		evaluated = NULL;
		return evaluated;
	}
	// if both are integer types, result is an int as well
	else if (resolved->type == integerType && resolved2->type == integerType) {
		currentValue = resolved->integerValue - resolved2->integerValue;
	}
	// if one is a float type, then result is a float type as well
	else if (resolved->type == floatType) {
		floatCheck = 1;
		if (resolved2->type == floatType) {
			currentValue = resolved->floatValue - resolved2->floatValue;
		}
		else if (resolved2->type == integerType) {
			currentValue = resolved->floatValue - resolved2->integerValue;
		}
		else {
			printf("syntax error -- second param is not a number\n");
			return NULL;
		}
	}
	else if (resolved->type == integerType && resolved2->type == floatType) {
		currentValue = resolved->integerValue - resolved2->floatValue;
		floatCheck = 1;
	}
	else {
		printf("syntax error -- non-numeric parameter\n");
		evaluated = NULL;
		return evaluated;
	}

	if (floatCheck) {
		evaluated->type = floatType;
		evaluated->floatValue = currentValue;
	}
	else {
		evaluated->type = integerType;
		evaluated->integerValue = currentValue;
	}
	return evaluated;
}

/*
Division
*/
Value *division(Value *currentParam, Value *env, int checker) {

	Value *evaluated = ourMalloc(sizeof(Value));
	
	float currentValue = 0;
	int floatCheck = 0;
	
	// checks for too few and too many parameters
	if (currentParam->type == endType || currentParam->cons->cdr->type == endType ||
			currentParam->cons->cdr->cons->cdr->type != endType) {
		printf("syntax error -- wrong number of parameters\n");
		return NULL;
	}
	
	Value *resolved = eval(currentParam, &env, checker);
	Value *resolved2 = eval(currentParam->cons->cdr, &env, checker);
	
	// couldn't find the variable, error has already been printed
	if (resolved == NULL || resolved2 == NULL) {
		evaluated = NULL;
		return evaluated;
	}
	// if both are integer types, result is an int as well
	else if (resolved->type == integerType && resolved2->type == integerType) {
		if (resolved2->integerValue == 0) {
			printf("syntax error -- divisor is 0\n");
			return NULL;
		}
		
		int f1 = resolved->integerValue;
		int f2 = resolved2->integerValue;
		if (f1 % f2 == 0) {
			currentValue = f1/f2;
		}
		else {
			floatCheck = 1;
			currentValue = ((f1/f2)*f2 + f1%f2)/(float)f2;
		}
	}
	// if one is a float type, then result is a float type as well
	else if (resolved->type == floatType) {
		floatCheck = 1;
		float f1 = resolved->floatValue;
		if (resolved2->type == floatType) {
			if (resolved2->floatValue == 0.0) {
				printf("syntax error -- divisor is 0\n");
				return NULL;
			}
			float f2 = resolved2->floatValue;
			currentValue = f1 / f2;
		}
		else if (resolved2->type == integerType) {
			if (resolved2->integerValue == 0) {
				printf("syntax error -- divisor is 0\n");
				return NULL;
			}
			int f2 = resolved2->integerValue;
			currentValue = f1 / f2;
		}
		else {
			printf("syntax error -- second param is not a number\n");
			return NULL;
		}
	}
	else if (resolved->type == integerType && resolved2->type == floatType) {
		int f1 = resolved->integerValue;
		if (resolved2->floatValue == 0.0) {
			printf("syntax error -- divisor is 0\n");
			return NULL;
		}
		float f2 = resolved2->floatValue;
		currentValue = f1 / f2;
		floatCheck = 1;
	}
	else {
		printf("syntax error -- non-numeric parameter\n");
		evaluated = NULL;
		return evaluated;
	}

	if (floatCheck) {
		evaluated->type = floatType;
		evaluated->floatValue = currentValue;
	}
	else {
		evaluated->type = integerType;
		evaluated->integerValue = currentValue;
	}
	return evaluated;
}

/*
Modulo
*/
Value *mod(Value *currentParam, Value *env, int checker) {

	Value *evaluated = ourMalloc(sizeof(Value));
	
	int currentValue = 0;
	
	// checks for too few and too many parameters
	if (currentParam->type == endType || currentParam->cons->cdr->type == endType ||
			currentParam->cons->cdr->cons->cdr->type != endType) {
		printf("syntax error -- wrong number of parameters\n");
		return NULL;
	}
	
	Value *resolved = eval(currentParam, &env, checker);
	Value *resolved2 = eval(currentParam->cons->cdr, &env, checker);
	
	// couldn't find the variable, error has already been printed
	if (resolved == NULL || resolved2 == NULL) {
		evaluated = NULL;
		return evaluated;
	}
	// if both are integer types, result is an int as well
	else if (resolved->type == integerType && resolved2->type == integerType) {
		if (resolved2->integerValue == 0) {
			printf("syntax error -- mod by 0 not allowed\n");
			return NULL;
		}
		currentValue = resolved->integerValue % resolved2->integerValue;
	}
	// can only evaluate integers
	else {
		printf("syntax error -- non-integer parameter\n");
		evaluated = NULL;
		return evaluated;
	}
	
	evaluated->type = integerType;
	evaluated->integerValue = currentValue;
	return evaluated;
}

/*
Less than or equal to
*/
Value *leq(Value *currentParam, Value *env, int checker) {

	Value *evaluated = ourMalloc(sizeof(Value));
	
	double currentValue = 0;
	
	// checks for too few and too many parameters
	if (currentParam->type == endType || currentParam->cons->cdr->type == endType ||
			currentParam->cons->cdr->cons->cdr->type != endType) {
		printf("syntax error -- wrong number of parameters\n");
		return NULL;
	}
	
	Value *resolved = eval(currentParam, &env, checker);
	Value *resolved2 = eval(currentParam->cons->cdr, &env, checker);
	
	// couldn't find the variable, error has already been printed
	if (resolved == NULL || resolved2 == NULL) {
		evaluated = NULL;
		return evaluated;
	}

	// need to know whether to access integerValue or floatValue
	else if (resolved->type == integerType && resolved2->type == integerType) {
		currentValue = (resolved->integerValue <= resolved2->integerValue);
	}
	else if (resolved->type == floatType) {
		if (resolved2->type == floatType) {
			currentValue = (resolved->floatValue <= resolved2->floatValue);
		}
		else if (resolved2->type == integerType) {
			currentValue = (resolved->floatValue <= resolved2->integerValue);
		}
		else {
			printf("syntax error -- second param is not a number\n");
			return NULL;
		}
	}
	else if (resolved->type == integerType && resolved2->type == floatType) {
		currentValue = (resolved->integerValue <= resolved2->floatValue);
	}
	else {
		printf("syntax error -- non-numeric parameter\n");
		evaluated = NULL;
		return evaluated;
	}

	evaluated->type = booleanType;
	evaluated->booleanValue = currentValue;
	return evaluated;
}

/*
Greater than or equal to
*/
Value *geq(Value *currentParam, Value *env, int checker) {
	Value *evaluated = ourMalloc(sizeof(Value));
	
	double currentValue = 0;
	
	// checks for too few and too many parameters
	if (currentParam->type == endType || currentParam->cons->cdr->type == endType ||
			currentParam->cons->cdr->cons->cdr->type != endType) {
		printf("syntax error -- wrong number of parameters\n");
		return NULL;
	}
	
	Value *resolved = eval(currentParam, &env, checker);
	Value *resolved2 = eval(currentParam->cons->cdr, &env, checker);
	
	// couldn't find the variable, error has already been printed
	if (resolved == NULL || resolved2 == NULL) {
		evaluated = NULL;
		return evaluated;
	}
	
	// need to know if should access integerValue of floatValue
	else if (resolved->type == integerType && resolved2->type == integerType) {
		currentValue = (resolved->integerValue >= resolved2->integerValue);
	}
	else if (resolved->type == floatType) {
		if (resolved2->type == floatType) {
			currentValue = (resolved->floatValue >= resolved2->floatValue);
		}
		else if (resolved2->type == integerType) {
			currentValue = (resolved->floatValue >= resolved2->integerValue);
		}
		else {
			printf("syntax error -- second param is not a number\n");
			return NULL;
		}
	}
	else if (resolved->type == integerType && resolved2->type == floatType) {
		currentValue = (resolved->integerValue >= resolved2->floatValue);
	}
	else {
		printf("syntax error -- non-numeric parameter\n");
		evaluated = NULL;
		return evaluated;
	}

	evaluated->type = booleanType;
	evaluated->booleanValue = currentValue;
	return evaluated;
}

/*
Equal to
*/
Value *eq(Value *currentParam, Value *env, int checker) {
	Value *evaluated = ourMalloc(sizeof(Value));
	
	double currentValue = 0;
	
	// checks for too few and too many parameters
	if (currentParam->type == endType || currentParam->cons->cdr->type == endType ||
			currentParam->cons->cdr->cons->cdr->type != endType) {
		printf("syntax error -- wrong number of parameters\n");
		return NULL;
	}
	
	Value *resolved = eval(currentParam, &env, checker);
	Value *resolved2 = eval(currentParam->cons->cdr, &env, checker);
	
	// couldn't find the variable, error has already been printed
	if (resolved == NULL || resolved2 == NULL) {
		evaluated = NULL;
		return evaluated;
	}

	// need to know whether to access integerValue or floatValue
	else if (resolved->type == integerType && resolved2->type == integerType) {
		currentValue = (resolved->integerValue == resolved2->integerValue);
	}
	else if (resolved->type == floatType) {
		if (resolved2->type == floatType) {
			currentValue = (resolved->floatValue == resolved2->floatValue);
		}
		else if (resolved2->type == integerType) {
			currentValue = (resolved->floatValue == resolved2->integerValue);
		}
		else {
			printf("syntax error -- second param is not a number\n");
			return NULL;
		}
	}
	else if (resolved->type == integerType && resolved2->type == floatType) {
		currentValue = (resolved->integerValue == resolved2->floatValue);
	}
	else {
		printf("syntax error -- non-numeric parameter\n");
		evaluated = NULL;
		return evaluated;
	}

	evaluated->type = booleanType;
	evaluated->booleanValue = currentValue;
	return evaluated;
}

/*
Greater than
*/
Value *gt(Value *currentParam, Value *env, int checker) {
	Value *evaluated = ourMalloc(sizeof(Value));
	
	double currentValue = 0;
	
	// checks for too few and too many parameters
	if (currentParam->type == endType || currentParam->cons->cdr->type == endType ||
			currentParam->cons->cdr->cons->cdr->type != endType) {
		printf("syntax error -- wrong number of parameters\n");
		return NULL;
	}
	
	Value *resolved = eval(currentParam, &env, checker);
	Value *resolved2 = eval(currentParam->cons->cdr, &env, checker);
	
	// couldn't find the variable, error has already been printed
	if (resolved == NULL || resolved2 == NULL) {
		evaluated = NULL;
		return evaluated;
	}
	// if both are integer types, result is an int as well
	else if (resolved->type == integerType && resolved2->type == integerType) {
		currentValue = (resolved->integerValue > resolved2->integerValue);
	}
	// if one is a float type, then result is a float type as well
	else if (resolved->type == floatType) {
		if (resolved2->type == floatType) {
			currentValue = (resolved->floatValue > resolved2->floatValue);
		}
		else if (resolved2->type == integerType) {
			currentValue = (resolved->floatValue > resolved2->integerValue);
		}
		else {
			printf("syntax error -- second param is not a number\n");
			return NULL;
		}
	}
	else if (resolved->type == integerType && resolved2->type == floatType) {
		currentValue = (resolved->integerValue > resolved2->floatValue);
	}
	else {
		printf("syntax error -- non-numeric parameter\n");
		evaluated = NULL;
		return evaluated;
	}

	evaluated->type = booleanType;
	evaluated->booleanValue = currentValue;
	return evaluated;
}

/*
Less than
*/
Value *lt(Value *currentParam, Value *env, int checker) {
	Value *evaluated = ourMalloc(sizeof(Value));
	
	double currentValue = 0;
	
	// checks for too few and too many parameters
	if (currentParam->type == endType || currentParam->cons->cdr->type == endType ||
			currentParam->cons->cdr->cons->cdr->type != endType) {
		printf("syntax error -- wrong number of parameters\n");
		return NULL;
	}
	
	Value *resolved = eval(currentParam, &env, checker);
	Value *resolved2 = eval(currentParam->cons->cdr, &env, checker);
	
	// couldn't find the variable, error has already been printed
	if (resolved == NULL || resolved2 == NULL) {
		evaluated = NULL;
		return evaluated;
	}
	// if both are integer types, result is an int as well
	else if (resolved->type == integerType && resolved2->type == integerType) {
		currentValue = (resolved->integerValue < resolved2->integerValue);
	}
	// if one is a float type, then result is a float type as well
	else if (resolved->type == floatType) {
		if (resolved2->type == floatType) {
			currentValue = (resolved->floatValue < resolved2->floatValue);
		}
		else if (resolved2->type == integerType) {
			currentValue = (resolved->floatValue < resolved2->integerValue);
		}
		else {
			printf("syntax error -- second param is not a number\n");
			return NULL;
		}
	}
	else if (resolved->type == integerType && resolved2->type == floatType) {
		currentValue = (resolved->integerValue < resolved2->floatValue);
	}
	else {
		printf("syntax error -- non-numeric parameter\n");
		evaluated = NULL;
		return evaluated;
	}

	evaluated->type = booleanType;
	evaluated->booleanValue = currentValue;
	return evaluated;
}

