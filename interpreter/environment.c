/* environment.c
* Functions for making / printing etc. environments
*  By Sophia Davis and Karen Halls
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "interpretFunctions.h"

/*
Adds a new frame to an environment -- for define and let*
*/
Value *addFrame(Value *currentCons, Value *env, Value *envToCheck, char *type) {

	ConsCell *cell = currentCons->cons;
	Value *evaluated;
		
	if (cell->car->type == consType && cell->cdr->type == endType) {
		env = addFrame(cell->car, env, envToCheck, type);
	}
	else if (cell->car->type == consType && cell->cdr->type == consType) {
		env = addFrame(cell->car, env, envToCheck, type);
		env = addFrame(cell->cdr, env, envToCheck, type);
	}
	
	// there will only be two things -- val cons and cons end
	else if (cell->car->type != consType && cell->cdr->type == consType) {

		if (cell->car->type != symbolType) {
			printf("syntax error\n");
			return env;
		}
		else {
			// let
			if (!strcmp(type, "let")) {
				evaluated = eval(cell->cdr, &envToCheck, 0);
				env = addBinding(evaluated, cell->car, env);
			}
			
			// letrec -- initialize variable names with value false
			else if (!strcmp(type, "letRecFalse")) {
				Value *false = ourMalloc(sizeof(Value));
				false->type = booleanType;
				false->booleanValue = 0;
				
				env = addBinding(false, cell->car, env);
			}
			
			// letrec -- then set vars with temp names to desired value
			else if (!strcmp(type, "letRecTemp")) {
				
				// make a "temp" value, with symbol type, where the symbol is a double string of the current symbol
				Value *dupSymbol = ourMalloc(sizeof(Value));
				dupSymbol->type = symbolType;
				
				char *second = ourMalloc(2*(strlen(cell->car->symbol)) + 2);
				strcpy(&second[0], cell->car->symbol);
				strcpy(&second[strlen(cell->car->symbol)], cell->car->symbol);
				dupSymbol->symbol = second;

				env = addBinding(cell->cdr->cons->car, dupSymbol, env);
			}
			
			// letrec -- last step -- change value associated with variable name (#f) to desired value
			else if (!strcmp(type, "set!")) {

				// fake consCell to pass in to resolved
				ConsCell *tempCell = ourMalloc(sizeof(ConsCell));
				
				Value *dupSymbol = ourMalloc(sizeof(Value));
				dupSymbol->type = symbolType;
				
				char *second = ourMalloc(2*(strlen(cell->car->symbol)) + 2);
				strcpy(&second[0], cell->car->symbol);
				strcpy(&second[strlen(cell->car->symbol)], cell->car->symbol);
				dupSymbol->symbol = second;
				
				tempCell->car = dupSymbol;
				
				Value *tempsVal = resolveVariable(tempCell, env);

				Value *voidVal = setBang(cell->car, tempsVal, env);
			}
			
			// let* or define
			else {
				
				evaluated = eval(cell->cdr, &env, 1);
				env = addBinding(evaluated, cell->car, env);
			}
		}
	}
	return env;
}

/*
Adds a binding to an environment frame
*/
Value *addBinding(Value *actual, Value *formal, Value *currentEnv) {
	
	Binding *newBinding = ourMalloc(sizeof(Binding));
	
	int size = strlen(formal->symbol);
	
	newBinding->name = ourMalloc(size*2 + 2);

	int i = 0;
	while(formal->symbol[i] != '\0') {
		newBinding->name[i] = formal->symbol[i];
		i++;
	}
	newBinding->name[i] = formal->symbol[i];
	
	if (actual == NULL) {
		return NULL;
	}
	
	switch(actual->type) {
		case consType:
			newBinding->val = actual;
			break;
		default:
			newBinding->val = memCopyValue(actual);
			break;
	}
	newBinding->nextBinding = currentEnv;
	
	Value *bindingPtr = ourMalloc(sizeof(Value));
	bindingPtr->type = bindingType;
	bindingPtr->nextBinding = newBinding;
	
	currentEnv = bindingPtr;
	return currentEnv;
}

/*
Prints bindings in current environment
*/
void printEnv(Value *env) {
	printf("The current environment: \n");

	Binding *binding;

	while (env->type != endType) {
		
		binding = env->nextBinding;
		
		printf("\n*Binding name: ");
		printf("%s -- ", binding->name);
		printf("value type is %i\n", binding->val->type);
		printValue(binding->val);
		printf("*\n");
		
		env = binding->nextBinding;
	}
}

/*
Resolves variables
*/
Value *resolveVariable(ConsCell *cell, Value *env) {

	Binding *binding;
	Value *toReturn;
	
	int found = 0;
	while(env->type != endType && !found) {
		
		binding = env->nextBinding;

		if (!strcmp(binding->name, cell->car->symbol)) {
			
			toReturn = ourMalloc(sizeof(Value));
			toReturn->type = binding->val->type;

			switch(binding->val->type) {
				case integerType:
					toReturn->integerValue = binding->val->integerValue;
					found = 1;
					break;
				case booleanType:
					toReturn->booleanValue = binding->val->booleanValue;
					found = 1;
					break;
				case stringType:
					toReturn->string = binding->val->string;
					found = 1;
					break;
				case floatType:
					toReturn->floatValue = binding->val->floatValue;
					found = 1;
					break;
				case symbolType:
					toReturn->symbol = binding->val->symbol;
					found = 1;
					break;
				case quoteType:
					toReturn = binding->val;
					found = 1;
					break;
				case consType:
					toReturn = binding->val;
					found = 1;
					break;
				case closureType:
					toReturn->closure = binding->val->closure;
					found = 1;
					break;
				case listType:
					toReturn = binding->val;
					found = 1;
					break;
				case pairType:
					toReturn = binding->val;
					found = 1;
					break;
				default:
					toReturn = NULL;
					return toReturn;
			}
			if (found) {
				return toReturn;
			}
		}
		env = binding->nextBinding;
	}
	if (!found) {
		printf("syntax error -- variable out of scope\n");
		toReturn = NULL;
	}
	return toReturn;
}

/*
set!
*/
Value *setBang(Value *varName, Value *newValue, Value *env) {
	Binding *binding;
	Value *toReturn;
	
	Value *bindingsBrandNewValue = eval(newValue, &env, 0);
	int found = 0;
		
	while(env->type != endType) {
		
		binding = env->nextBinding;
		
		if (!strcmp(binding->name, varName->symbol)) {
			
			toReturn = ourMalloc(sizeof(Value));
			toReturn->type = voidType;
			binding->val->type = bindingsBrandNewValue->type;
			
			switch(bindingsBrandNewValue->type) {
				case integerType:
					binding->val->integerValue = bindingsBrandNewValue->integerValue;
					found = 1;
					break;
				case booleanType:
					binding->val->booleanValue = bindingsBrandNewValue->booleanValue;
					found = 1;
					break;
				case stringType:
					binding->val->string = bindingsBrandNewValue->string;
					found = 1;
					break;
				case floatType:
					binding->val->floatValue = bindingsBrandNewValue->floatValue;
					found = 1;
					break;
				case symbolType:
					binding->val->symbol = bindingsBrandNewValue->symbol;
					found = 1;
					break;
				case quoteType:
					binding->val = bindingsBrandNewValue;
					found = 1;
					break;
				case consType:
					binding->val = bindingsBrandNewValue;
					found = 1;
					break;
				case closureType:
					binding->val->closure = bindingsBrandNewValue->closure;
					found = 1;
					break;
				case listType:
					binding->val = bindingsBrandNewValue;
					found = 1;
					break;
				case pairType:
					binding->val = bindingsBrandNewValue;
					found = 1;
					break;
				default:
					binding->val = NULL;
					return NULL;
			}
			if (found) {
				return toReturn;
			}
		}
		env = binding->nextBinding;
	}
	if (!found) {
		printf("syntax error -- variable out of scope\n");
		toReturn = NULL;
	}
	return toReturn;
}

/*
Removes local variables from scope
*/
Value **freeFrame(Value **envPointer, int checker) {
	Value *env = *envPointer;
	Binding *binding;
	while (env->type != endType && env->type != endFrame) {
		binding = env->nextBinding;
		env = binding->nextBinding;
	}
	*envPointer = env;
	return envPointer;
}

/*
Combines environment with reference frame from closures
*/
Value *mergeInRefFrame(Value *refFrame, Value *env) {
	Value *origFrame;
	
	if (env->type == endType) {
		origFrame = refFrame;
	}
	else if (refFrame->type == endType || refFrame->type == endFrame) {
		origFrame = env;
	}
	else {
		
		origFrame = ourMalloc(sizeof(Value));
		origFrame->type = bindingType;
		
		Value *firstOrigFrame = origFrame;
		
		Binding *binding;
	
		while (!(refFrame->type == endFrame || refFrame->type == endType)) {
			binding = refFrame->nextBinding;
			
			origFrame->nextBinding = ourMalloc(sizeof(Binding));
			memcpy(origFrame->nextBinding, binding, sizeof(Binding));
			
			origFrame->nextBinding->nextBinding = ourMalloc(sizeof(Value));
			memcpy(origFrame->nextBinding->nextBinding, binding->nextBinding, sizeof(Value));
			
			refFrame = binding->nextBinding;
			if (refFrame->type == endFrame || refFrame->type == endType) {
				origFrame->nextBinding->nextBinding = env;
			}
			else {
				origFrame = origFrame->nextBinding->nextBinding;
			}
		}
		return firstOrigFrame;
	}
	return origFrame;
}