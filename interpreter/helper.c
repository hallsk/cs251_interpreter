/* helper.c
* Assorted helper functions
*  By Sophia Davis and Karen Halls
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "interpretFunctions.h"

/*
Finds next ConsCell with a value
*/
ConsCell *getNextCell(Value *expr) {
	
	ConsCell *cell;
	if (expr->cons->car->type == consType && expr->cons->cdr->type == endType) {
		cell = expr->cons->car->cons;
		if (cell->car->type == consType) {
			cell = getNextCell(cell->car);
		}
	}
	else if (expr->cons->car->type == consType && expr->cons->cdr->type == consType) {
		cell = expr->cons->car->cons;
		if (cell->car->type == consType) {
			cell = getNextCell(cell->car);
		}
	}
	else if (expr->cons->car->type != consType && expr->cons->cdr->type == consType) {
		cell = expr->cons;
	}
	else if (expr->cons->car->type != consType && expr->cons->cdr->type == endType) {
		cell = expr->cons;
	}
	return cell;
}

/*
Prints values
*/
void printValue(Value *eval) {
	
	char *escaped;
	
	switch(eval->type) {
		case openType:
			printf("%c ", eval->openParen);
			break;
		case closeType:
			printf("%c ", eval->closeParen);
			break;
        case endType:
        	printf(") ");
            break;
		case consType:
			printTree(eval);
			break;
		case stringType:
			escaped = escape(eval->string);
			printf("\"%s\" ", escaped);
			break;
		case integerType:
			printf("%i ", eval->integerValue);
			break;
		case floatType:
			printf("%f ", eval->floatValue);
			break;
		case symbolType:
			printf("%s ", eval->symbol);
			break;
		case quoteType:
			printTree(eval);
			break;
		case booleanType:
			if (eval->booleanValue == 1) {
				printf("#t ");
			}
			else {
				printf("#f ");
			}
			break;
		case closureType:
			printf("<#.procedure>");
			break;
		case pairType:
			printf("(");
			printValue(eval->cons->car);
			printf(" . ");
			printValue(eval->cons->cdr);
			printf(")");
			break;
		case listType:
			if (eval->cons->car->type == pairType || eval->cons->cdr->type == pairType ||
				eval->cons->car->type == listType || eval->cons->cdr->type == listType) {
					if (eval->cons->car->type == listType) {
						printf("(");
					}
					printValue(eval->cons->car);
					if (eval->cons->car->type == listType) {
						printf(")");
					}
					printValue(eval->cons->cdr);
			}
			else {
				printTree(eval);
			}
			break;
		default:
			break;
	}
}

/*
Determines whether a value is 'truthy' (i.e. a value other than #f, and not 0).
Returns a Value with booleanValue set to 1 or 0.
*/
Value *truthy(Value *toCheck) {
	Value *result = ourMalloc(sizeof(Value));
	result->type = booleanType;
	
	if (toCheck->type == booleanType) {
		result->booleanValue = toCheck->booleanValue;
	}
	else if (toCheck->type == integerType) {
		if (toCheck->integerValue == 0) {
			result->booleanValue = 0;
		}
		else {
			result->booleanValue = 1;
		}
	}
	else {
		result->booleanValue = 1;
	}

	return result;
}



