#include "../parser/parseFunctions.h"

/*
Implemented in interpreter.c
*/
Value *eval(Value *expr, Value **envPointer, int checker);

Value *evalNaked(Value *nakedExpr, Value *env);

/*
Implemented in eval_helper.c
*/
Value *evalIf(Value *expr, Value *env);

Value *evalQuote(Value *expr);

Value *addClosure(Value *params, Value *body, Value *currentEnv);

Value *addLet(Value *currentParam, Value **envPointer, int checker, char *type);

Value *addLetRec(Value *currentParam, Value **envPointer, int checker);

Value *evalAnd(Value *currentParam, Value *env, int checker);

Value *evalOr(Value *currentParam, Value *env, int checker);

Value *evalBegin(Value *currentParam, Value *env, int checker);

Value *evalCond(Value *currentParam, Value *env, int checker);

/*
Implemented in environment.c
*/
Value *addFrame(Value *currentCons, Value *env, Value *envToCheck, char *type);

Value *addBinding(Value *actual, Value *formal, Value *currentEnv);

void printEnv(Value *env);

Value *resolveVariable(ConsCell *cell, Value *env);

Value *setBang(Value *varName, Value *newValue, Value *env);

Value **freeFrame(Value **envPointer, int checker);


/*
Implemented in helper.c
*/
ConsCell *getNextCell(Value *expr);

void printValue(Value *eval);

Value *mergeInRefFrame(Value *refFrame, Value *env);

Value *truthy(Value *toCheck);

/*
Implemented in primitives.c
*/
Value *null(Value *list);

Value *car(Value *list);

Value *cdr(Value *list);

Value *evalCons(Value *currentTree, Value *env, int checker);

Value *evalPlus(Value *currentParam, Value *env, int checker);

Value *mult(Value *currentParam, Value *env, int checker);

Value *subtraction(Value *currentParam, Value *env, int checker);

Value *division(Value *currentParam, Value *env, int checker);

Value *mod(Value *currentParam, Value *env, int checker);

Value *leq(Value *currentParam, Value *env, int checker);

Value *geq(Value *currentParam, Value *env, int checker);

Value *eq(Value *currentParam, Value *env, int checker);

Value *gt(Value *currentParam, Value *env, int checker);

Value *lt(Value *currentParam, Value *env, int checker);