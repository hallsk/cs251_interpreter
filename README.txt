/*
A Scheme interpreter, written in C
Karen Halls and Sophia Davis
for Programming Languages, Winter 2014, Carleton College
*/


The following functions and special forms are implemented in our interpreter:

define
let
let*
letrec
set!

lambda

if -- with a consequence and an alternative
cond

and
or
begin

null?
car
cdr
cons

+, -, *, /, modulo, >, <, =, <=, >=

Other notes:
Our implementation of Scheme defines both #f and 0 as "falsey."
Recursion works most of the time.
There's something wrong with our code and we have no idea what it is :(


Here is a lambda for you:
                                                                                
                                                                                
                     ..OMMMMMMI                                                 
                   .$MMMMMMMMMMMM=                                              
                  ~MMMMMMMMMMMMMMMM.                                            
                 +MMMMMMMMMMMMMMMMMM,                                           
                .MMMM? .    . MMMMMMM                                           
                MMM?            MMMMMM                                          
                MM:             .MMMMM~.                                        
               ,MM.               MMMMM                                         
               :ZI                 MMMMM.                                       
                                   .MMMM.                                       
                                    ZMMMM                                       
                                     MMMM,                                      
                                    .8MMMM.                                     
                                     MMMMM,                                     
                                   .MMMMMMM                                     
                                   NMMMMMMM=                                    
                                  MMMMMMMMMM                                    
                                 MMMMMMMMMMM~                                   
                                MMMMMMMMMMMMM                                   
                               MMMMMMMMMMMMMM:                                  
                              MMMMMMMMMM 7MMMM                                  
                             MMMMMMMMMM   MMMM.                                 
                            MMMMMMMMMM    OMMMM                                 
                           MMMMMMMMMM     .MMMM~                                
                          MMMMMMMMMM.     .IMMMM                                
                         MMMMMMMMMM,        MMMM,                               
                        MMMMMMMMMM          ZMMMM                               
                      .MMMMMMMMMM            MMMM,                              
                     .MMMMMMMMMM.            OMMMM                              
                     MMMMMMMMMM:              MMMM8                ::           
                    MMMMMMMMMM                DMMMMN              .MM           
                  .MMMMMMMMMM=                 MMMMMI            .NMM           
                 .MMMMMMMMMM?                  ~MMMMMM          .NMMM.          
                .MMMMMMMMMMI                    DMMMMMMM8,    ~NMMMM7           
                MMMMMMMMMM:                     .MMMMMMMMMMMMMMMMMMM.           
              =MMMMMMMMMM+                       .8MMMMMMMMMMMMMMMZ.            
             =MMMMMMMMMMI                           +MMMMMMMMMMM7               
                                       .              .  =?+:                   
                          NMI       7MMMM        .MM.              MM.          
                         .MMN     .NMM ?MM       ?MMMM.          MMMM~          
                         .MMN    .MMM   ?MM~     ?MMMMMM, ... ,MMMMMM~          
         .+              .MMN   .MMM.    .MMZ    ?MM. MMMMMMMMMMM .MM~          
         =MM.             MM:   MMD. .MM.  MMM   ?MM.   .     .   .MM~          
         .MMMMMMMMMMMMMMMMMM  ~MM?   OMM   .MMM  ?MM.    .MMM.    .MM~          
            MMMMMMMMMMMMM8    ~M.     .      MM   MO     .MMM.     =M           
                                                                            

